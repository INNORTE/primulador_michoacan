module EscenariosHelper
  # Constructor del contador de las restricciones de las elecciones
  # tomando en cuenta que en cada bloque debe de existir paridad de género
  # y de manera global 1 de cada 3 debe tener una edad menor a 35 años al día
  # de las elecciones
  def calculate_bloques
    bloques = obtain_n_bloques
    objetos = obtain_n_obj_bloques bloques
    @escenario.tipo_escenario === 2 ? (candidatos_formateados = obtain_candidatos_dist(bloques,objetos)) : (candidatos_formateados = obtain_candidatos_mpio(bloques, objetos))
    return candidatos_formateados
  end

  # Obtener el número de bloques que existe en la base de datos tomando en
  # cuenta el objeto seleccionado, es decir Municipios o Distritos (ambos
  # deben estar descritos como bloques, en caso de no estarlo, se debe de
  # contar como un solo bloque general)
  # Parámetros:
  #   - @datos: Esta variable contiene todos los objetos (mpios o dist) con
  #     su bloque declarado
  #
  # Retorna:
  #   - bloques: este arreglo contiene los bloques que están declarados
  #     en base de datos por parte de los objetos. Ej. [1,2,3]
  def obtain_n_bloques
    bloques = []
    @datos.map do |dato|
      bloques << dato.bloque unless dato.bloque.in? bloques
    end
    return bloques.sort
  end

  def obtain_n_obj_bloques bloques
    objetos = Array.new(bloques.length,0)
    @datos.map do |dato|
      objetos[dato.bloque - 1] += 1
    end
    return objetos
  end

  # Obtiene el número de candidatos separados por bloques y por edad para su
  # posterior formateo a codigo HTML.
  # Parámetros:
  #   - bloques: este arreglo contiene los bloques en los que se
  #     encuentran los objetos en la base de datos.
  #
  # Retorna:
  #   - candidatos_bloques: Este hash contiene el número de
  #   candidatos separados por bloques y a su vez por sexo, para cumplir con
  #   la regla 50/50 en cada bloque, además de contar la edad de cada candidato
  #   para cumplir con la regla 1/3 < 35 años
  #   Ej. {"1":{hombres: 15, mujeres: 16},
  #        "2":{hombres: 4, mujeres: 10}
  #        "1-3":{candidatos: 1, meta: 30}}
  def obtain_candidatos_mpio bloques, objetos
    candidatos_bloques = initialize_hash_bloques bloques, objetos
    @candidatos.map do |candidato|
      candidato.candidato.sexo ? (candidatos_bloques[:"#{candidato.candidato.carto_mpio.bloque}"][:hombres] += 1) : (candidatos_bloques[:"#{candidato.candidato.carto_mpio.bloque}"][:mujeres] += 1)
      edad = obtain_edad_candidato candidato.candidato_id
      candidatos_bloques[:"1-3"][:candidatos] += 1 if edad <= 35
    end
    return candidatos_bloques
  end

  def obtain_candidatos_dist bloques, objetos
    candidatos_bloques = initialize_hash_bloques bloques, objetos
    @candidatos.map do |candidato|
      candidato.candidato.sexo ? (candidatos_bloques[:"#{candidato.carto_dist.bloque}"][:hombres] += 1) : (candidatos_bloques[:"#{candidato.carto_dist.bloque}"][:mujeres] += 1)
      edad = obtain_edad_candidato candidato.candidato_id
      candidatos_bloques[:"1-3"][:candidatos] += 1 if edad <= 35
    end
    return candidatos_bloques
  end

  # Devuelve un hash formateado para su posterior actualización de valores
  # de cada candidato, tomando en cuenta su sexo y su edad. En esta función
  # solo se retorna el "cascarón" del cálculo.
  # Parámetros:
  #   - bloques: Array de los bloques que contiene los bloques en los que se
  #     encuentran los objetos en la base de datos.
  #
  # Retorna:
  #   - hash: Hash formateado para su posterior actualización de valores.
  #   Ej. {"1":{hombres: 0, mujeres: 0},
  #        "2":{hombres: 0, mujeres: 0}
  #        "1-3":{candidatos: 0, meta: 30}}
  def initialize_hash_bloques bloques, objetos
    hash = {}
    # raise @datos.length.inspect
    bloques.map do |bloque|
      temporal_hash = {"#{bloque}": {"mujeres": 0, "hombres": 0, "total": (objetos[bloque - 1])}}
      hash.merge!(temporal_hash)
    end
    return hash.merge!({"1-3": {"candidatos": 0, "meta": (@datos.length / 3 + (@datos.length%3 > 0 ? 1 : 0)).round}})
  end

  # Calcula la edad del candidato hasta el día de la elección
  # Parámetros:
  #   - candidato_id: identificador único del candidato.
  #
  # Retorna:
  #   - edad: años de edad del candidato calculado al día de la elección.
  def obtain_edad_candidato candidato_id
    return Candidato.select("date_part('year',age(date('2017-07-01'),fecha_nacimiento)) as edad").where("id = ?", candidato_id).first.edad
  end

  def formatear_html candidatos_formateados
    text = ''
    valid = false
    candidatos_formateados.map do |cf|
      cf.first.to_s.eql?("1-3") ? valid = rule_13_valid?(cf.last) : valid = block_valid?(cf.last)
      text += "<div class='col s12 m6 l3 padding'><div><div class='title #{valid ? 'correct' : 'wrong'} center'><h5>#{cf.first.to_s}</h5><div class='bloques'><table class='centered'>"
      header = '<tr>'
      column = '<tr>'
      cf.last.map do |c|
        header += "<th>#{c.first.to_s.capitalize}</th>"
        column += "<td id='#{c.first.to_s}_#{cf.first.to_s}'>#{c.last.to_s}</td>"
      end
      header += '</tr>'
      column += '</tr>'
      text += header + column + '</table></div></div></div></div>'
    end
    return text.html_safe
  end

  def block_valid? bloque
    parametro = ((bloque[:total] - 1).to_f / 2).round
    ((bloque[:hombres] >= parametro) && (bloque[:mujeres] >= parametro) && ((bloque[:hombres] + bloque[:mujeres]) === bloque[:total] )) ? (return true) : (return false)
  end

  def rule_13_valid? datos
    datos[:candidatos] >= datos[:meta] ? (return true) : (return false)
  end

  def generate_notas_bloque candidatos_formateados
    mensaje = ''
    candidatos_formateados.map do |cf|
      unless cf.first.to_s.eql?("1-3")
        block_valid?(cf.last) ? (mensaje += "<div class='row correct'><h5 class='w-margin'>Bloque #{cf.first.to_s}:</h5><p class='w-margin'>Se cumple la regla de equidadd de género</p></div>") : (mensaje += get_recomendation_per_block(cf))
      else
        rule_13_valid?(cf.last) ? (mensaje += "<div class='row correct'><h5 class='w-margin'>Regla 1/3</h5><p class='w-margin'>Se cumple la regla 1 de cada 3 deben ser menores a 35 años</p></div>") : (mensaje += get_recomendation_13(cf.last))
      end
    end
    return mensaje
  end
  # 38 Total  20mujeres 3 hombres   38/2=19; 3-20=-17; 19-3=16
  # 37 total  1 mujer   30 hombres  37/2=18; 30-1=29; 18-1=17
  def get_recomendation_per_block bloque
    mensaje = "<div class='row wrong'><h5 class='w-margin'>Bloque #{bloque.first.to_s}:</h5><p class='w-margin'>Falta por asignar "
    cumple_total = (bloque.last[:hombres] + bloque.last[:mujeres] == bloque.last[:total])
    if cumple_total
      valor = bloque.last[:hombres] - bloque.last[:mujeres]
      valor < 0 ? (mensaje += "#{valor.abs/2} #{valor.abs/2 > 1 ? 'hombres' : 'hombre' }.") : (mensaje += "#{valor.abs/2} #{valor.abs/2 > 1 ? 'mujeres' : 'mujer' }.")
    elsif bloque.last[:hombres] > (bloque.last[:total] + bloque.last[:total] % 2) || bloque.last[:mujeres] > (bloque.last[:total] + bloque.last[:total] % 2)
      valor = bloque.last[:hombres] - bloque.last[:mujeres]
      paridad = bloque.last[:total] / 2
      extra = bloque.last[:total] % 2
      valor < 0 ? (mensaje += "#{paridad + extra - bloque[:hombres]} #{paridad + extra - bloque[:hombres] > 1 ? "hombres" : "hombre" }") : (mensaje += "#{paridad + extra - bloque[:mujeres]} #{paridad + extra - bloque[:mujeres] > 1 ? "mujeres" : "mujer" }")
    else
      # raise bloque.inspect
      mensaje += "#{bloque.last[:total] / 2 - bloque.last[:hombres]} #{(bloque.last[:total] / 2 - bloque.last[:hombres]) > 1 ? 'hombres' : 'hombre'} y
      #{bloque.last[:total] / 2 + bloque.last[:total] % 2 - bloque.last[:mujeres]} #{(bloque.last[:total] / 2 + bloque.last[:total] % 2  - bloque.last[:mujeres]) > 1 ? 'mujeres' : 'mujeres'}"
    end
    mensaje += "</div>"
  end

  def get_recomendation_13 bloque
    faltantes = bloque[:meta] - bloque[:candidatos]
    mensaje = "<div class='row wrong'><h5 class='w-margin'>Regla 1/3</h5>"
    mensaje += "<p class='w-margin'>Falta por asignar #{faltantes} #{faltantes > 1 ? "candidatos" : "candidato"}.</p>"
    mensaje += "</div>"
    return mensaje
  end
end
