class HistoricoEscenarioDist < ApplicationRecord
  belongs_to :escenario
  belongs_to :candidato
  belongs_to :carto_dist

  def self.calculate_votos_totales_escenario escenario_id
    HistoricoEscenarioDist.group(:escenario_id).where("escenario_id = ?", escenario_id).sum(:indice)
  end
end
