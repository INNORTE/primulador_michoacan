class HistoricoEscenarioMpio < ApplicationRecord
  belongs_to :escenario
  belongs_to :candidato
  belongs_to :carto_mpio

  def self.calculate_votos_totales_escenario escenario_id
    HistoricoEscenarioMpio.group(:escenario_id).where("escenario_id = ?", escenario_id).sum(:indice)
  end
end
