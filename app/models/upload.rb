class Upload < ApplicationRecord
  require 'csv'
  has_attached_file :csv

  validates_attachment :csv, presence: true, content_type: {content_type: "text/plain"}, size: { in: 0..2.megabytes}

  def self.create_candidatos_csv id
    csv = File.read(Upload.find(id).csv.path)
    csv_parse = CSV.parse(csv, headers: true, encoding: 'utf-8', col_sep: '|')
    csv_parse.map do |c|
      # candidatos = Candidato.where("nombre = ?",c["nombre"].mb_chars.titleize)
      # if candidatos.empty?
        cand = Candidato.new
        cand.nombre = c["nombre"].mb_chars.titleize
        cand.fecha_nacimiento = Date::strptime(c["fecha_nacimiento"], "%d-%m-%Y")
        c["sexo"].upcase.eql?("M") || c["sexo"].upcase.eql?("MASCULINO") ? cand.sexo = true : cand.sexo = false
        cand.encuesta = c["encuesta"]
        cand.carto_mpio_id = c["carto_mpio_id"]
        cand.valid? ? cand.save : (return (c["nombre"].mb_chars.titleize))
      # elsif candidatos.count == 1
      #   candidatos.update(nombre: c["nombre"].mb_chars.titleize,
      #     fecha_nacimiento: Date::strptime(c["fecha_nacimiento"], "%d-%m-%Y"),
      #     sexo: c["sexo"]
      #   )
      # end
    end
    return true
  end

end
