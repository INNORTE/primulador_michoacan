class CartoMpio < ApplicationRecord
  has_and_belongs_to_many :carto_dists
  has_many :candidato
  has_many :indices_mpio
end
