class Escenario < ApplicationRecord
  belongs_to :user
  has_many :historico_escenario_dists
  has_many :historico_escenario_mpios

  validates :nombre, presence: true, uniqueness: true
  validates :tipo_escenario, presence: true, if: :it_is_under_parameters?

  def it_is_under_parameters?
    tipo_escenario.in?([1,2])
  end
end
