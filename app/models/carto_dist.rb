class CartoDist < ApplicationRecord
  has_and_belongs_to_many :carto_mpios
  has_many :indices_dist
end
