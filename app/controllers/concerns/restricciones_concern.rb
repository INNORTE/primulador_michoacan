module RestriccionesConcern
  extend ActiveSupport::Concern

  def obtain_data_for_bloques escenario_id
    @escenario = Escenario.find(escenario_id)
    if @escenario.tipo_escenario === 1
      @datos = CartoMpio.select('id,nom_mun as municipio, edo as entidad, bloque, padron_electoral as "padron electoral", votantes_pri as "PRI votantes"').where('activo = ?', true)
      @candidatos = HistoricoEscenarioMpio.select('id, escenario_id, candidato_id, carto_mpio_id, indice').where("escenario_id = ?", escenario_id);
    else
      @datos = CartoDist.select('id,nom_dist, bloque, padron_electoral as "padron electoral", votantes_pri as "PRI votantes"')
      @candidatos = HistoricoEscenarioDist.select('id, escenario_id, candidato_id, carto_dist_id, indice').where("escenario_id = ?", escenario_id);
    end
    return calculate_bloques
  end

  # Constructor del contador de las restricciones de las elecciones
  # tomando en cuenta que en cada bloque debe de existir paridad de género
  # y de manera global 1 de cada 3 debe tener una edad menor a 35 años al día
  # de las elecciones
  # Parámetros:
  #  - @datos: Los mpios/distritos para calcular los bloques
  #  descritos en base de datos.
  #  - @candidatos: Candidatos asginados en cada escenarios, para calcular el
  #  escenario actual
  #
  #  Retorna:
  #  - formatear_html: Bloques y candidatos asignados en base de datos con su
  #  respectivo voto

  def calculate_bloques
    bloques = obtain_n_bloques
    objetos = obtain_n_obj_bloques bloques
    @escenario.tipo_escenario === 2 ? (candidatos_formateados = obtain_candidatos_dist(bloques,objetos)) : (candidatos_formateados = obtain_candidatos_mpio(bloques, objetos))
    return formatear_html candidatos_formateados
  end

  # Obtener el número de bloques que existe en la base de datos tomando en
  # cuenta el objeto seleccionado, es decir Municipios o Distritos (ambos
  # deben estar descritos como bloques, en caso de no estarlo, se debe de
  # contar como un solo bloque general)
  # Parámetros:
  #   - @datos: Esta variable alberga todas los objetos (mpios o dist) con
  #     su bloque declarado
  #
  # Retorna:
  #   - bloques: este arreglo contiene los bloques que están declarados
  #     en base de datos por parte de los objetos. Ej. [1,2,3]
  def obtain_n_bloques
    bloques = []
    @datos.map do |dato|
      bloques << dato.bloque unless dato.bloque.in? bloques
    end
    return bloques
  end

  def obtain_n_obj_bloques bloques
    objetos = Array.new(bloques.length,0)
    @datos.map do |dato|
      objetos[dato.bloque - 1] += 1
    end
    return objetos
  end

  # Obtiene el número de candidatos separados por bloques y por edad para su
  # posterior formateo a codigo HTML.
  # Parámetros:
  #   - bloques: este arreglo contiene los bloques en los que se
  #     encuentran los objetos en la base de datos.
  #
  # Retorna:
  #   - candidatos_bloques: Este hash contiene el número de
  #   candidatos separados por bloques y a su vez por sexo, para cumplir con
  #   la regla 50/50 en cada bloque, además de contar la edad de cada candidato
  #   para cumplir con la regla 1/3 < 35 años
  #   Ej. {"1":{hombres: 15, mujeres: 16},
  #        "2":{hombres: 4, mujeres: 10}
  #        "1-3":{candidatos: 1, meta: 30}}
  def obtain_candidatos_mpio bloques, objetos
    candidatos_bloques = initialize_hash_bloques bloques, objetos
    @candidatos.map do |candidato|
      candidato.candidato.sexo ? (candidatos_bloques[:"#{candidato.candidato.carto_mpio.bloque}"][:hombres] += 1) : (candidatos_bloques[:"#{candidato.candidato.carto_mpio.bloque}"][:mujeres] += 1)
      edad = obtain_edad_candidato candidato.candidato_id
      candidatos_bloques[:"1-3"][:candidatos] += 1 if edad <= 35
    end
    return candidatos_bloques
  end

  def obtain_candidatos_dist bloques, objetos
    candidatos_bloques = initialize_hash_bloques bloques, objetos
    @candidatos.map do |candidato|
      candidato.candidato.sexo ? (candidatos_bloques[:"#{candidato.carto_dist.bloque}"][:hombres] += 1) : (candidatos_bloques[:"#{candidato.carto_dist.bloque}"][:mujeres] += 1)
      edad = obtain_edad_candidato candidato.candidato_id
      candidatos_bloques[:"1-3"][:candidatos] += 1 if edad <= 35
    end
    return candidatos_bloques
  end

  # Devuelve un hash formateado para su posterior actualización de valores
  # de cada candidato, tomando en cuenta su sexo y su edad. En esta función
  # solo se retorna el "cascarón" del cálculo.
  # Parámetros:
  #   - bloques: Array de los bloques que contiene los bloques en los que se
  #     encuentran los objetos en la base de datos.
  #
  # Retorna:
  #   - hash: Hash formateado para su posterior actualización de valores.
  #   Ej. {"1":{hombres: 0, mujeres: 0},
  #        "2":{hombres: 0, mujeres: 0}
  #        "1-3":{candidatos: 0, meta: 30}}
  def initialize_hash_bloques bloques, objetos
    hash = {}
    # raise @datos.length.inspect
    bloques.map do |bloque|
      temporal_hash = {"#{bloque}": {"mujeres": 0, "hombres": 0, "total": (objetos[bloque - 1])}}
      hash.merge!(temporal_hash)
    end
    return hash.merge!({"1-3": {"candidatos": 0, "meta": (@datos.length / 3).round}})
  end

  # Calcula la edad del candidato hasta el día de la elección
  # Parámetros:
  #   - candidato_id: identificador único del candidato.
  #
  # Retorna:
  #   - edad: años de edad del candidato calculado al día de la elección.
  def obtain_edad_candidato candidato_id
    return Candidato.select("date_part('year',age(date('2017-07-01'),fecha_nacimiento)) as edad").where("id = ?", candidato_id).first.edad
  end

  def formatear_html candidatos_formateados
    text = ''
    valid = false
    candidatos_formateados.map do |cf|
      cf.first.to_s.eql?("1-3") ? valid = rule_13_valid?(cf.last) : valid = block_valid?(cf.last)
      text += "<div class='col s12 m6 l3 padding'><div class='hoverable'><div class='title #{valid ? 'correct' : 'wrong'} center'><h5>#{cf.first.to_s}</h5><div class='bloques'><table class='centered'>"
      header = '<tr>'
      column = '<tr>'
      cf.last.map do |c|
        header += "<th>#{c.first.to_s.capitalize}</th>"
        column += "<td id='#{c.first.to_s}_#{cf.first.to_s}'>#{c.last.to_s}</td>"
      end
      header += '</tr>'
      column += '</tr>'
      text += header + column + '</table></div></div></div></div>'
    end
    return text.html_safe
  end

  def block_valid? bloque
    parametro = ((bloque[:total] - 1).to_f / 2).round
    ((bloque[:hombres] >= parametro) && (bloque[:mujeres] >= parametro) && ((bloque[:hombres] + bloque[:mujeres]) === bloque[:total] )) ? (return true) : (return false)
  end

  def rule_13_valid? datos
    datos[:candidatos] >= datos[:meta] ? (return true) : (return false)
  end
end
