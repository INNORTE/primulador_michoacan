class AjaxController < ApplicationController
  before_action :find_escenario, only: [:asignar_candidato_objeto_escenario]

  include RestriccionesConcern

  def buscar_candidato
    # raise params.inspect
    params[:tipo_escenario].eql?("1") ? (@candidatos = get_candidatos_mpio) : (@candidatos = get_candidatos_dist)
    respond_to do |f|
      f.json {render json: @candidatos}
      f.xml {render xml: @candidatos}
    end
  end

  def asignar_candidato_objeto_escenario
    creado = false
    @escenario.tipo_escenario === 1 ? creado = anadir_candidato_mpio : creado = anadir_candidato_dist
    if creado
      @escenario.tipo_escenario === 1 ? (cand = obtener_candidato_con_bloque_mpio params[:candidato_id]) : (cand = obtener_candidato_con_bloque_dist params[:candidato_id], params[:objeto_id])
      respond_to do |f|
        f.json { render json: {success: true, data: cand}}
        f.xml { render xml: {success: true, data: cand}}
      end
    else
      respond_to do |f|
        f.json { render json: {success: false}}
        f.xml { render xml: {success: false}}
      end
    end
  end

  def get_restricciones_html
    restricciones = obtain_data_for_bloques params[:escenario_id]
    respond_to do |f|
      f.html { render html: restricciones}
      f.json { render json: {success: true, html: restricciones}}
    end
  end

  private
  def get_candidatos_mpio
    Candidato.select("id, nombre, date_part('year',age(date('2018-07-01'),fecha_nacimiento)) as edad, sexo, encuesta").where("carto_mpio_id = ?",params[:objeto_id])
  end

  def get_candidatos_dist
    municipios = CartoDist.find(params[:objeto_id]).carto_mpio_ids
    candidatos = []
    municipios.map do |m|
      candidatos_arr = Candidato.select("id, nombre, date_part('year',age(date('2018-07-01'),fecha_nacimiento)) as edad, sexo, encuesta").where("carto_mpio_id = ?",m)
      candidatos_arr.map do |c|
        candidatos << c
      end
    end
    candidatos
  end

  def find_escenario
    @escenario = Escenario.find(params[:escenario_id])
  end

  def anadir_candidato_mpio
    registro = HistoricoEscenarioMpio.where("escenario_id = ? and carto_mpio_id = ?",params[:escenario_id],params[:objeto_id])
    unless registro.empty?
      registro.map do |r|
        r.destroy
      end
    end
    votos = (CartoMpio.find(params[:objeto_id]).votantes_pri * ( 1 + ((( Candidato.find(params[:candidato_id]).encuesta * 20 ).to_f / 100 ).to_f / 100 ))).round
    hem = HistoricoEscenarioMpio.new(escenario_id: params[:escenario_id], candidato_id: params[:candidato_id], carto_mpio_id: params[:objeto_id], indice: votos)
    hem.save ? (return true) : (return false)
  end

  def anadir_candidato_dist
    registro = HistoricoEscenarioDist.where("escenario_id = ? and carto_dist_id = ?",params[:escenario_id],params[:objeto_id])
    unless registro.empty?
      registro.map do |r|
        r.destroy
      end
    end
    votos = (CartoDist.find(params[:objeto_id]).votantes_pri * ( 1 + ((( Candidato.find(params[:candidato_id]).encuesta * 20 ).to_f / 100 ).to_f / 100 ))).round
    hed = HistoricoEscenarioDist.new(escenario_id: params[:escenario_id], candidato_id: params[:candidato_id], carto_dist_id: params[:objeto_id], indice: votos)
    hed.save ? (return true) : (return false)
  end

  def obtener_candidato_con_bloque_mpio candidato_id
    return Candidato.joins(:carto_mpio).select("candidatos.id, candidatos.nombre, date_part('year',age(date('2018-07-01'),fecha_nacimiento)) as edad, candidatos.sexo, candidatos.encuesta, candidatos.carto_mpio_id as objeto, carto_mpios.bloque").where("candidatos.id = ?",params[:candidato_id]).first
  end

  def obtener_candidato_con_bloque_dist candidato_id, id_dist
    candidato = Candidato.select("id, nombre, date_part('year',age(date('2018-07-01'),fecha_nacimiento)) as edad, sexo, encuesta, carto_mpio_id, carto_mpio_id as objeto").find(params[:candidato_id])
    bloque = candidato.carto_mpio.carto_dists.find(id_dist).bloque
    return candidato.as_json.merge({bloque: bloque})
  end

end
