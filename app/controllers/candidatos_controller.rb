class CandidatosController < ApplicationController
  before_action :authenticate_user!
  before_action :get_all_candidatos, only: [:index]
  before_action :find_candidato, only: [:edit,:destroy,:update]
  before_action :get_all_mpios, only: [:new,:edit]
  def index
  end

  def new
    @candidato = Candidato.new
  end

  def create
    @candidato = Candidato.new(candidato_params)
    if @candidato.save
      respond_to do |f|
        f.html { redirect_to candidatos_path, notice: "Candidato creado" }
        f.json { render json: {created: true}}
      end
    else
      respond_to do |f|
        f.html { redirect_to :new, alert: @candidato.errors.full_messages }
        f.json { render json: {created: false, reason: @candidato.errors.full_messages}}
      end
    end
  end

  def show
  end

  def edit
  end

  def update
    if @candidato.update(candidato_params)
      respond_to do |f|
        f.html { redirect_to candidatos_path, notice: "Se actualizó el candidato #{@candidato.nombre.mb_chars.titleize}"}
      end
    else
      respond_to do |f|
        f.html {redirect_to edit_candidato(@candidato.id), alert: @candidato.errors.full_messages }
      end
    end
  end

  def destroy
    @candidato.update(activo: false)
    respond_to do |f|
      f.html { redirect_to candidatos_path}
    end
  end

  def create_candidatos_csv
    if Candidato.add_csv params[:csv_candidatos]
      respond_to do |f|
        f.html {redirect_to candidatos_path, notice: "Los candidatos fueron importados correctamente"}
      end
    else
      respond_to do |f|
        f.html {redirect_to candidatos_path, alert: @candidatos.errors.full_messages}
      end
    end
  end

  private
  def candidato_params
    params.require(:candidato).permit(:nombre,:fecha_nacimiento,:sexo,:carto_mpio_id,:encuesta)
  end

  def get_all_candidatos
    @candidatos = Candidato.select("id, nombre, fecha_nacimiento, date_part('year',age(date('2018-07-01'),fecha_nacimiento)) as edad,sexo,encuesta,carto_mpio_id").where("activo = true")
  end

  def find_candidato
    @candidato = Candidato.find(params[:id])
  end

  def get_all_mpios
    @mpios = CartoMpio.all
  end
end
