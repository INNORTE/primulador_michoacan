class UploadController < ApplicationController
  def create
    begin
      upload = Upload.new(csv: params[:csv_candidatos])
      if upload.save
        validos = Upload.create_candidatos_csv upload.id
        if validos
          flash[:notice] = ["Todos los candidatos fueron creados correctamente."]
        else
          raise ArgumentError.new("El candidato #{validos} no se encuentra correctamente formateado, favor de corregirlo.")
        end
        redirect_to candidatos_path
      end
    rescue Exception => msg
      respond_to do |f|
        f.html { redirect_to(candidatos_path, alert: ["Algunos candidatos no se crearon debido a errores. Error: #{msg.message}"])}
      end
    end
  end
end
