class EscenariosController < ApplicationController
  before_action :authenticate_user!
  rescue_from ActiveRecord::RecordNotFound, :with => :escenario_not_found
  before_action :get_escenarios_activos, only:[:index]
  before_action :new_escenario, only:[:new]
  before_action :find_escenario, except: [:index,:new,:create,:create_escenario_optimo_dist_prueba]
  before_action :is_active_escenario, only:[:edit,:show,:update]
  before_action :get_data_carto, only: [:show,:edit]

  def index
  end

  def new
  end

  def create
    @escenario = Escenario.new(escenario_params)
    @escenario.user_id = current_user.id
    if @escenario.save
      create_escenario_optimo
      redirect_to edit_escenario_path(@escenario.id)
    else
      flash[:alert] = @escenario.errors.full_messages
      redirect_to new_escenario_path
    end

  end

  def show
    @edit=false
    respond_to do |f|
      f.html
      f.json
      f.pdf do
        render pdf:'reporte',template:'escenarios/reporte', page_size: 'Letter'
      end
    end
  end

  def edit
    @edit=true
  end

  def update
  end

  def destroy
    if @escenario.update(activo: false)
      flash[:notice] = "Escenario dado de baja con éxito"
      redirect_to root_path
    else
      flash[:alert] = "Ocurrió un problema con la eliminación del escenario"
      redirect_to root_path
    end
  end

  private
  def get_escenarios
    @escenarios = Escenario.where("user_id = ?", current_user.id)
  end

  def get_escenarios_activos
    @escenarios = Escenario.where("activo = ? and user_id = ?", true, current_user.id)
  end

  def find_escenario
    @escenario = Escenario.find(params[:id])
  end

  def get_data_carto
    if @escenario.tipo_escenario === 1
      @datos = CartoMpio.select('id,bloque,nom_mun as municipio, edo as entidad, votantes_pri as "PRI votantes"').where('activo = ?',true)
      @candidatos = HistoricoEscenarioMpio.select('id, escenario_id, candidato_id, carto_mpio_id as "objeto_id",carto_mpio_id, indice').where("escenario_id = ?", params[:id]);
    else
      @datos = CartoDist.select('id, bloque, cve_dist as "no. dist",nom_dist as "Nombre Distrito", votantes_pri as "PRI votantes"')
      @candidatos = HistoricoEscenarioDist.select('id, escenario_id, candidato_id, carto_dist_id as "objeto_id",carto_dist_id, indice').where("escenario_id = ?", params[:id]);
    end
  end

  def new_escenario
    @escenario = Escenario.new
  end

  def escenario_params
    return params.require(:escenario).permit(:nombre,:descripcion,:tipo_escenario)
  end

  def is_active_escenario
    unless @escenario.activo
      flash[:alert] = ["Escenario no se encuentra activo","favor de pedirle al admin que lo active"]
      redirect_to root_path
    end
    unless @escenario.user_id === current_user.id
      flash[:alert] = ["No tienes acceso a ese escenario.","Ese escenario le pertenece a otro usuario."]
      redirect_to root_path
    end
  end

  def escenario_not_found
    flash[:alert] = ["Escenario no encontrado, probablemente no exista"]
    redirect_to root_path
  end

  def create_escenario_optimo
    @escenario.tipo_escenario === 1 ? create_escenario_optimo_mpio(@escenario.id) : create_escenario_optimo_dist(@escenario.id)
  end

  def create_escenario_optimo_mpio escenario_id
    mpios = CartoMpio.where('activo = true').order(votantes_pri: :desc)
    candidatos_asignados = []
    mpios.map do |m|
      candidatos = Candidato.where("carto_mpio_id = ?", m.id).order(encuesta: :desc)
      candidatos.map do |c|
        unless c.id.in? candidatos_asignados
          candidatos_asignados << c.id
          indice = (m.votantes_pri * ( 1 + ((( c.encuesta * 20 ).to_f / 100 ).to_f / 100 ))).round
          HistoricoEscenarioMpio.create(escenario_id: escenario_id, candidato_id: c.id, carto_mpio_id: m.id, indice: indice)
          break
        end
      end
    end
  end

  def create_escenario_optimo_dist escenario_id
    dist = CartoDist.all.order(votantes_pri: :desc)
    candidatos_asignados = []
    dist.map do |d|
      mpios = d.carto_mpios
      condicion = ""
      mpios.each_with_index do |m,i|
        condicion += "carto_mpio_id = #{m.id}"
        condicion += " or " unless i === (mpios.count - 1)
      end
      candidatos = Candidato.where(condicion).order(encuesta: :desc)
      candidatos.map do |c|
        unless c.id.in? candidatos_asignados
          candidatos_asignados << c.id
          indice = (d.votantes_pri * ( 1 + ((( c.encuesta * 20 ).to_f / 100 ).to_f / 100 ))).round
          HistoricoEscenarioDist.create(escenario_id: escenario_id, candidato_id: c.id, carto_dist_id: d.id, indice: indice)
          break
        end
      end
    end
  end
end
