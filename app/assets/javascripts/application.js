// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require materialize
//= require sweetalert2/dist/sweetalert2.all.min.js
//= require vanilla-datatables
//= require jquery-flexdatalist

document.addEventListener('turbolinks:load', function(){
  initialize();
});

var Electo = {
  _url_: "/electo_michoacan",
  refreshPage: function(){
    location.reload();
  },
  Showable : {
    init: function(activators){
      activators.forEach(val => {
        return val.onclick = Electo.Showable.show;
      });
    },
    show: function(e){
      let show_element = document.getElementById(e.target.getAttribute('data-in'));
      e.target.hasAttribute('data-out') && Electo.Showable.hide(e.target.getAttribute('data-out'));
      show_element.classList.contains('hide') && Electo.Showable.showing(show_element);
      e.target.hasAttribute('data-assign') ?
        Electo.Showable.assign(document.getElementById(e.target.getAttribute('data-assign')),e.target.getAttribute('data-value')) :
        Electo.Showable.assign(document.getElementById(e.target.getAttribute('data-assign')),'');
      return ;
    },
    hide: function(id){
      let hide_element = document.getElementById(id);
      !hide_element.classList.contains('hide') && Electo.Showable.hideing(hide_element);
      return ;
    },
    showing: function(element){
      element.classList.add('fadein');
      setTimeout(function(){
        element.classList.remove('hide');
        element.classList.remove('fadein');
      },450);
      return ;
    },
    hideing: function(element){
      element.classList.add('fadeout');
      setTimeout(function(){
        element.classList.add('hide')
        element.classList.remove('fadeout');
      },450);
      return ;
    },
    assign: function(element,value){
      element.value = value;
    }
  },
  Ajax:{
    send: function(url,method,parameters = '',functionTrue = Electo.Utils.nothing,functionFalse = Electo.Utils.nothing){
      Rails.ajax({
        type: method,
        url: url,
        data: parameters,
        success: function(response){functionTrue(response)},
        error: function(response){ functionFalse(response)}
      });
    },
  },
  Messages:{
    init: function(activators){
      activators.forEach(act => {
        return act.onclick = Electo.Messages.preShowMessage;
      });
    },
    preShowMessage:function(e){
      if(e.target.hasAttribute("data-method-http")){
        switch (e.target.getAttribute("data-method-http")) {
          case "DELETE":
            Electo.Messages.showDeleteMessage(e.target);
            break;
          case "POST":
            Electo.Messages.showPostMessage(e.target);
            break;
          case "GET":
            Electo.Messages.showGetMessage(e.target);
            break;
          case "UPDATE":
            Electo.Messages.showUpdateMessage(e.target);
            break;
        }
      }else{
        // Aquí se debe de mandar un msj neutral
        Electo.ShowMessage();
        return console.log('jejeje, saludos!');
      }
    },
    showDeleteMessage: function(button){
      let options = {
        title: button.getAttribute("data-title-message"),
        type: 'question',
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      };
      swal(options).then(result => {
        if(result.value){
          swal({
            title:'Eliminando',
            text:'El escenario se está intentando eliminar',
            onOpen: () => {
              swal.showLoading();
              Electo.Ajax.send(`${button.getAttribute('data-delete-url')}`,
                button.getAttribute('data-method-http'),
              );
            },
          });
        } else if(result.dismiss === 'cancel'){
          swal({
            title: "Escenario no eliminado",
            text: "El escenario no fué eliminado",
            type: "error"
          });
        }
      });
    },
    showMessageCandidatoFailed: function(reason){
      swal({
        title: "Candidato no creado",
        text: "Estos son los errores: " + reason,
        type: "error"
      });
    },
    showMessageCandidatoCreated: function(){
      swal({
        title: "El candidato ha sido creado",
        text: "El candidato se ha creado con éxito",
        type:"success",
        button: "Entendido"
      }).then((val)=>{
        Electo.Form.resetFields(document.getElementById('buscarCandidatoTab'));
      });
      return true;
    },
    showMessage:function(options){
      swal(options);
    }
  },
  Table:{
    init: function(element){
      Electo.Table.table = new DataTable(element,{perPageSelect: false});
    },
    table: '',
  },
  Form:{
    showModalCandidato: function(id_objeto){
      let mpio_field = document.getElementById('carto_mpio_id');
      Electo.Form._id_objeto_ = id_objeto;
      mpio_field && (mpio_field.value = id_objeto);
      $('#modalCandidato').modal('open');
      Electo.Form.activateSearchCandidato();
    },
    _tipo_escenario_: '',
    _id_objeto_: '',
    _id_escenario_: '',
    activateSearchCandidato:function(){
      // Aqui debo de gestionar un ajax pidiendo la información referente a los candidatos y su distrito/mpio para que flexdatalist no haga la petición
      $('.flexdatalist').attr('data-url',`/electo_michoacan/ajax/buscar_candidato/${Electo.Form._tipo_escenario_}/${Electo.Form._id_objeto_}.json`);
      $('.flexdatalist').flexdatalist({url: `/electo_michoacan/ajax/buscar_candidato/${Electo.Form._tipo_escenario_}/${Electo.Form._id_objeto_}.json`,cache: false}).on('select:flexdatalist', function(e, set, options) {
        // mostrar modal anunciando que los datos del candidato están en la siguiente pestaña
        swal({
          title:'Datos cargados del Candidato',
          text:'Se cargaron los datos del candidato en la pestaña "Datos del candidato"',
          type:"success",
          confirmButtonText: 'Ver los datos'}).then((result) =>{
            $('ul.tabs').tabs('select_tab', 'datosCandidatoTab');
          });
        Electo.Form.setValuesOnModal(set);
      });
    },
    setValuesOnModal: function(cand){
      let texto_formateado = `<p>Nombre: <b>${cand.nombre}</b></p><p>Sexo: ${cand.sexo ? 'Masculino' : 'Femenino'}</p><p>Edad: ${cand.edad} años</p><p>Porcentaje de aceptación (Encuesta): ${cand.encuesta}%</p><button type="button" data-nombre-candidato="${cand.nombre}" data-id-candidato="${cand.id}" name="button" class="btn btn-large red waves waves-light right" onclick="Electo.Form.addCandidatoToHistorical(this)">Seleccionar</button>`;
      document.getElementById('datosCandidatoSeleccionado').innerHTML = texto_formateado;
    },
    addCandidatoToHistorical: function(nombre){
      let electo = Electo.Form;
      Electo.Ajax.send(`/electo_michoacan/ajax/asignar_candidato/${electo._id_escenario_}/${electo._id_objeto_}/${nombre.getAttribute('data-id-candidato')}`,'POST',electo.verifyCandidatoAddedToHistorical,electo.verifyCandidatoAddedToHistorical);
    },
    verifyCandidatoAddedToHistorical: (response) => {
      response.success ? Electo.Form.addCandidatoToView(response.data) : Electo.Mensajes.showMessage({title:'Algo salió mal...',text:'No se añadió el candidato por problemas con el servidor',type: 'error'});
      return ;
    },
    addCandidatoToView: (candidato) => {
      Electo.Form.addCandidatoToCell(candidato.nombre);
      Electo.Form.addTotalVotos(Electo.Form._id_objeto_, candidato.encuesta);
      swal({title: 'Datos añadidos', text: 'El total de votos se ha calculado', type:'success'}).then(() => {
        $('#modalCandidato').modal('close');
      });
      // Aquí falta actualizar la parte de las restricciones por medio de la edad y sexo
      Electo.Restrictions.evaluateRestriction();
    },
    addTotalVotos: (objeto,encuesta) => {
      let total_div = document.getElementById(`total_votos_${objeto}`),total_pri = document.getElementById(`pri_votantes_${objeto}`), total = Number(total_pri.innerText);
      total_div.innerText = Math.round(total * (1 + (((encuesta*20)/100)/100)));
      return ;
    },
    addCandidatoToCell: function(nombre){
      let electo = Electo.Form;
      document.getElementById(`candidato_${Electo.Form._id_objeto_}`).innerHTML = `<p>${nombre}</p>`;
      return ;
    },
    clearAllInputs: function(id_busqueda, id_datos){
      let busqueda = document.getElementById(id_busqueda), datos = document.getElementById(id_datos);
      Electo.Form.resetFields(busqueda);
      datos.innerHTML = '';
      datos.innerHTML = '<h5>Estos son los datos del candidato</h5><div id="datosCandidatoSeleccionado"><p>Actualmente no se ha seleccionado algún candidato.</p><p>Favor de seleccionar un candidato en la pestaña anterior.</p></div>';
      $('ul.tabs').tabs('select_tab', 'buscarCandidatoTab');
    },
    resetFields: (e) =>{
      let child = {};
      if(e.nodeName === "DIV" || e.nodeName === "FORM" || e.nodeName ===  "P"){
        child = e.children
        Object.keys(child).forEach(function(k){
          Electo.Form.resetFields(child[k]);
        });
      }else if(e.nodeName === "INPUT"){
        if(e.type === 'text' || e.type === 'number' || e.type === 'date' ){
          Electo.Form.resetText(e);
        }else if(e.type === 'radio'){
          Electo.Form.resetRadios(e);
        }
      }
      return ;
    },
    resetCheckbox: function(e){
      if(e.checked == true && e.onchange != null){
        e.click();
      }else{
        e.checked = false;
      }
      return ;
    },
    resetRadios: function(e){
      e.checked = false;
      return ;
    },
    resetText: function(e){
      e.value = '';
      return ;
    },
  },
  Restrictions:{
    evaluateRestriction: () => {
      let restricciones = Electo.Ajax.send(`/electo_michoacan/ajax/obtener_restricciones_html/${Electo.Form._id_escenario_}.json`,'GET','',Electo.Restrictions.assignRestrictions,Electo.Restrictions.verifyRestrictions);
      return ;
    },
    verifyRestrictions: (resp) => {
      resp.success ? Electo.Restrictions.assignRestrictions(resp) : swal({title:'Restricciones no actualizadas',text:'Por algún motivo no se actualizó el panel de restricciones',type:'error'})
    },
    assignRestrictions: (resp) => {
      let restrictions_div = document.getElementById('restriccionesBloques');
      restrictions_div.innerHTML = resp.html;
    }
  },
  Utils: {
    nothing: function(){
      return ;
    }
  }
};
