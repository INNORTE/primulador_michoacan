# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'
csv_carto_mpio_route = File.read(Rails.root.join('lib','seeds','mpios_elecciones.csv'))
csv_carto_dist_route = File.read(Rails.root.join('lib','seeds','distritos_elecciones.csv'))
csv_relaciones_dist_mpio_route = File.read(Rails.root.join('lib','seeds','relaciones_distritos_mpios.csv'))
csv_candidatos = File.read(Rails.root.join('lib','seeds','candidatos.csv'))

csv_carto_mpio = CSV.parse(csv_carto_mpio_route, headers: true, encoding: 'utf-8', col_sep: '|')
csv_carto_dist = CSV.parse(csv_carto_dist_route, headers: true, encoding: 'utf-8', col_sep: '|')
csv_dist_mpio = CSV.parse(csv_relaciones_dist_mpio_route, headers: true, encoding: 'utf-8', col_sep: '|')
csv_cand = CSV.parse(csv_candidatos, headers: true,encoding: 'utf-8', col_sep: '|')

csv_carto_mpio.map do |cm|
  cmp = CartoMpio.new
  cmp.id = cm['id']
  cmp.nom_mun = cm['nom_mun']
  cmp.padron_electoral = cm['padron_electoral']
  cmp.votantes_pri = cm['votantes_pri']
  cmp.bloque = cm['bloque']
  cmp.save
end

csv_carto_dist.map do |cd|
  cdp = CartoDist.new
  cdp.id = cd['id']
  cdp.cve_dist = cd['cve_dist']
  cdp.nom_dist = cd['nom_dist']
  cdp.padron_electoral = cd['padron_electoral']
  cdp.votantes_pri = cd['votantes_pri']
  cdp.bloque = cd['bloque']
  cdp.save
end

csv_dist_mpio.map do |dm|
  cd = CartoDist.find(dm['carto_dist_id'])
  mpios = dm['carto_mpio_ids'].split(',')
  cd.carto_mpio_ids = mpios
end

csv_cand.map do |c|
  cn = Candidato.new
  cn.nombre = c['nombre'].mb_chars.titleize
  cn.fecha_nacimiento = Date::strptime(c["fecha_nacimiento"], "%d-%m-%Y")
  c["sexo"].upcase.eql?("M") || c["sexo"].upcase.eql?("MASCULINO") ? cn.sexo = true : cn.sexo = false
  cn.encuesta = c["encuesta"]
  cn.carto_mpio_id = c["carto_mpio_id"]
  if(cn.valid?)
    cn.save
  else
    print "Candidato #{cn.nombre}: #{cn.errors.full_messages}"
    break
  end
end

CartoMpio.find(113).update!(activo: false)

User.create!(email: "simulador@pri.mx", password: "51mu14d0r_M")
User.create!(email: "privado@pri.mx", password: "Pr1v4d0_M.")


# 1_000.times do |i|
#   Candidato.create!(nombre: Faker::Name.name_with_middle,fecha_nacimiento: Faker::Time.between(65.years.ago, Date.today),sexo: [true, false].sample, encuesta: rand(0.0..100.0).round(2),carto_mpio_id: rand(1..112))
# end

# CartoDist.create(cve_dist: "1", nom_dist: "Distrito 1", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "2", nom_dist: "Distrito 2", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "3", nom_dist: "Distrito 3", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "4", nom_dist: "Distrito 4", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "5", nom_dist: "Distrito 5", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "6", nom_dist: "Distrito 6", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "7", nom_dist: "Distrito 7", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "8", nom_dist: "Distrito 8", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoDist.create(cve_dist: "9", nom_dist: "Distrito 9", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
#
# CartoMpio.create(nom_mun: "Los Reyes", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "Morelia", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 1", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 2", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 3", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 4", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 5", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 6", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
# CartoMpio.create(nom_mun: "mpio 7", padron_electoral: 1500, votantes_pri: 600, bloque: 1)
#
# CartoDist.find(1).carto_mpio_ids=[1,2]
# CartoDist.find(2).carto_mpio_ids=[1,3]
# CartoDist.find(3).carto_mpio_ids=[4,1,3,2]
# CartoDist.find(4).carto_mpio_ids=[9,7,5]
# CartoDist.find(5).carto_mpio_ids=[9,8,6]
# CartoDist.find(6).carto_mpio_ids=[6]
# CartoDist.find(7).carto_mpio_ids=[8]
# CartoDist.find(8).carto_mpio_ids=[9]
# CartoDist.find(9).carto_mpio_ids=[5,6,7]
#
# Candidato.create(nombre: "Enrique García Pacheco", fecha_nacimiento: "1993-10-15",sexo: true, encuesta: 0.0, carto_mpio_id: 2)
# Candidato.create(nombre: "Celia Teresa Navarro Jáuregui", fecha_nacimiento: "1987-10-21",sexo: false, encuesta: 0.0, carto_mpio_id: 1)
# Candidato.create(nombre: "Junior Iván Panuco", fecha_nacimiento: "1980-01-10",sexo: true, encuesta: 0.0, carto_mpio_id: 3)
# Candidato.create(nombre: "Giovanni Daniel Rosales Gómez", fecha_nacimiento: "1990-11-10",sexo: true, encuesta: 30.8, carto_mpio_id: 4)
# Candidato.create(nombre: "Esmeralda Gómez Pérez", fecha_nacimiento: "1966-02-07",sexo: false, encuesta: 9.9, carto_mpio_id: 5)
# Candidato.create(nombre: "Jorge Ortíz", fecha_nacimiento: "1978-05-29",sexo: true, encuesta: 5.0, carto_mpio_id: 6)
# Candidato.create(nombre: "Abraham Machuca Vázquez", fecha_nacimiento: "1987-03-14",sexo: true, encuesta: 18.0, carto_mpio_id: 7)
# Candidato.create(nombre: "Victor Vargas Fonseca", fecha_nacimiento: "1989-02-14",sexo: true, encuesta: 15.0, carto_mpio_id: 8)
# Candidato.create(nombre: "Lesly Anahí Burgara", fecha_nacimiento: "1985-09-15",sexo: false, encuesta: 45.5, carto_mpio_id: 9)
# Candidato.create(nombre: "Janeth Amara Aguilar", fecha_nacimiento: "1984-04-20",sexo: true, encuesta: 35.5, carto_mpio_id: 1)
