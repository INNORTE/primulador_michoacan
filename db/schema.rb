# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180201163631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "candidatos", force: :cascade do |t|
    t.string "nombre", null: false
    t.datetime "fecha_nacimiento", null: false
    t.boolean "sexo", null: false
    t.float "encuesta", default: 0.0, null: false
    t.bigint "carto_mpio_id"
    t.boolean "activo", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["carto_mpio_id"], name: "index_candidatos_on_carto_mpio_id"
  end

  create_table "carto_dists", force: :cascade do |t|
    t.string "cve_dist", null: false
    t.string "nom_dist", null: false
    t.integer "padron_electoral", null: false
    t.integer "votantes_pri", null: false
    t.integer "bloque", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "carto_dists_mpios", id: false, force: :cascade do |t|
    t.bigint "carto_dist_id", null: false
    t.bigint "carto_mpio_id", null: false
    t.index ["carto_dist_id"], name: "index_carto_dists_mpios_on_carto_dist_id"
    t.index ["carto_mpio_id"], name: "index_carto_dists_mpios_on_carto_mpio_id"
  end

  create_table "carto_mpios", force: :cascade do |t|
    t.string "nom_mun", null: false
    t.string "edo", default: "Michoacán de Ocampo", null: false
    t.integer "padron_electoral", null: false
    t.integer "votantes_pri", null: false
    t.integer "bloque", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "activo", default: true
  end

  create_table "escenarios", force: :cascade do |t|
    t.string "nombre"
    t.integer "tipo_escenario"
    t.text "descripcion"
    t.boolean "activo", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_escenarios_on_user_id"
  end

  create_table "historico_escenario_dists", force: :cascade do |t|
    t.bigint "escenario_id"
    t.bigint "candidato_id"
    t.bigint "carto_dist_id"
    t.float "indice"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["candidato_id"], name: "index_historico_escenario_dists_on_candidato_id"
    t.index ["carto_dist_id"], name: "index_historico_escenario_dists_on_carto_dist_id"
    t.index ["escenario_id"], name: "index_historico_escenario_dists_on_escenario_id"
  end

  create_table "historico_escenario_mpios", force: :cascade do |t|
    t.bigint "escenario_id"
    t.bigint "candidato_id"
    t.bigint "carto_mpio_id"
    t.float "indice"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["candidato_id"], name: "index_historico_escenario_mpios_on_candidato_id"
    t.index ["carto_mpio_id"], name: "index_historico_escenario_mpios_on_carto_mpio_id"
    t.index ["escenario_id"], name: "index_historico_escenario_mpios_on_escenario_id"
  end

  create_table "indices_dists", force: :cascade do |t|
    t.float "leal"
    t.float "volatil"
    t.float "competitivo"
    t.bigint "carto_dist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["carto_dist_id"], name: "index_indices_dists_on_carto_dist_id"
  end

  create_table "indices_mpios", force: :cascade do |t|
    t.float "leal"
    t.float "volatil"
    t.float "competitivo"
    t.bigint "carto_mpios_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["carto_mpios_id"], name: "index_indices_mpios_on_carto_mpios_id"
  end

  create_table "uploads", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "csv_file_name"
    t.string "csv_content_type"
    t.integer "csv_file_size"
    t.datetime "csv_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "candidatos", "carto_mpios"
  add_foreign_key "escenarios", "users"
  add_foreign_key "historico_escenario_dists", "candidatos"
  add_foreign_key "historico_escenario_dists", "carto_dists"
  add_foreign_key "historico_escenario_dists", "escenarios"
  add_foreign_key "historico_escenario_mpios", "candidatos"
  add_foreign_key "historico_escenario_mpios", "carto_mpios"
  add_foreign_key "historico_escenario_mpios", "escenarios"
  add_foreign_key "indices_dists", "carto_dists"
  add_foreign_key "indices_mpios", "carto_mpios", column: "carto_mpios_id"
end
