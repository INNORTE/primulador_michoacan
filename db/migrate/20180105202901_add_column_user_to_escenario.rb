class AddColumnUserToEscenario < ActiveRecord::Migration[5.1]
  def change
    add_reference :escenarios, :user, foreign_key: true 
  end
end
