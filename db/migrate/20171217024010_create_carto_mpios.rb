class CreateCartoMpios < ActiveRecord::Migration[5.1]
  def change
    create_table :carto_mpios do |t|
      t.string :nom_mun, null: false
      t.string :edo, null: false, default: "Michoacán de Ocampo"
      t.integer :padron_electoral, null: false
      t.integer :votantes_pri, null: false
      t.integer :bloque, null: false

      t.timestamps
    end
  end
end
