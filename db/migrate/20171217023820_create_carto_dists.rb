class CreateCartoDists < ActiveRecord::Migration[5.1]
  def change
    create_table :carto_dists do |t|
      t.string :cve_dist, null: false
      t.string :nom_dist, null: false
      t.integer :padron_electoral, null: false
      t.integer :votantes_pri, null: false
      t.integer :bloque, null: false

      t.timestamps
    end
  end
end
