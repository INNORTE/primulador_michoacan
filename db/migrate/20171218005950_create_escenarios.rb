class CreateEscenarios < ActiveRecord::Migration[5.1]
  def change
    create_table :escenarios do |t|
      t.string :nombre
      t.integer :tipo_escenario
      t.text :descripcion
      t.boolean :activo, null: false, default: true

      t.timestamps
    end
  end
end
