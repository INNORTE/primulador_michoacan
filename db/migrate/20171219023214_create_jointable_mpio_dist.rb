class CreateJointableMpioDist < ActiveRecord::Migration[5.1]
  def change
    create_join_table :carto_dists, :carto_mpios, table_name: 'carto_dists_mpios' do |t|
      t.index :carto_dist_id
      t.index :carto_mpio_id
    end
  end
end
