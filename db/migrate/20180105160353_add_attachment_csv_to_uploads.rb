class AddAttachmentCsvToUploads < ActiveRecord::Migration[5.1]
  def up
    change_table :uploads do |t|
      t.attachment :csv
    end
  end

  def down
    remove_attachment :uploads, :csv
  end
end
