class CreateCandidatos < ActiveRecord::Migration[5.1]
  def change
    create_table :candidatos do |t|
      t.string :nombre, null: false
      t.datetime :fecha_nacimiento, null: false
      t.boolean :sexo, null: false
      t.float :encuesta, null: false, default: 0
      t.references :carto_mpio, foreign_key: true
      t.boolean :activo, null: false, default: true

      t.timestamps
    end
  end
end
