class CreateHistoricoEscenarioMpios < ActiveRecord::Migration[5.1]
  def change
    create_table :historico_escenario_mpios do |t|
      t.references :escenario, foreign_key: true
      t.references :candidato, foreign_key: true
      t.references :carto_mpio, foreign_key: true
      t.float :indice

      t.timestamps
    end
  end
end
