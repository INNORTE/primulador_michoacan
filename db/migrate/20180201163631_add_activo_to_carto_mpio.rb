class AddActivoToCartoMpio < ActiveRecord::Migration[5.1]
  def change
    add_column :carto_mpios, :activo, :boolean, default: true
  end
end
