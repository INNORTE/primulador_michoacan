class CreateIndicesMpios < ActiveRecord::Migration[5.1]
  def change
    create_table :indices_mpios do |t|
      t.float :leal
      t.float :volatil
      t.float :competitivo
      t.references :carto_mpios, foreign_key: true

      t.timestamps
    end
  end
end
