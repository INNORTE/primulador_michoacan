class AddPostgisToDb < ActiveRecord::Migration[5.1]
  def up
    execute %{
      set search_path to public;
      create extension postgis;
      set search_path to sc_electoral;
    }
  end

  def down
    execute %{
      set search_path to public;
      drop extension postgis;
      set search_path to sc_electoral;
    }
  end
end
