class CreateIndicesDists < ActiveRecord::Migration[5.1]
  def change
    create_table :indices_dists do |t|
      t.float :leal
      t.float :volatil
      t.float :competitivo
      t.references :carto_dist, foreign_key: true

      t.timestamps
    end
  end
end
