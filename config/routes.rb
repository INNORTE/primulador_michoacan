Rails.application.routes.draw do
  root 'escenarios#index'
  devise_for :users, controllers: {sessions: 'users/sessions'}
  get 'ajax/buscar_candidato/:tipo_escenario/:objeto_id', to: "ajax#buscar_candidato"
  get 'ajax/obtener_restricciones_html/:escenario_id', to: 'ajax#get_restricciones_html'
  post 'ajax/asignar_candidato/:escenario_id/:objeto_id/:candidato_id/', to: "ajax#asignar_candidato_objeto_escenario"
  post 'candidatos/upload_csv', to: 'upload#create', as: 'upload_candidatos_csv'
  resources :candidatos
  resources :escenarios
end
