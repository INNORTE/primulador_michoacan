require 'test_helper'

class EscenariosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get escenarios_index_url
    assert_response :success
  end

  test "should get new" do
    get escenarios_new_url
    assert_response :success
  end

  test "should get show" do
    get escenarios_show_url
    assert_response :success
  end

  test "should get edit" do
    get escenarios_edit_url
    assert_response :success
  end

end
